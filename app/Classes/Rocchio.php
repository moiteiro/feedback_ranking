<?php

namespace App\Classes;

use File;

class Rocchio {

    protected $documents;
    protected $evaluations;
    protected $stopwords;

    public function __construct ($documents, $evaluations)
    {
        $this->documents = $documents;
        $this->evaluations = $evaluations;
        $this->stopwords = File::getRequire(storage_path().'/app/stopwords.php');

        foreach ($this->documents as $key => $document) {
            $this->documents[$key]->dictionary = $this->_createDictionary($document->content);
        }

        foreach ($this->evaluations as $key => $evaluation) {
            $document = $this->evaluations[$key]->document;
            if ($document != NULL)
                $this->evaluations[$key]->document->dictionary = $this->_createDictionary($document->content);
        }
    }

    public function calculateSimilarity() 
    {
        foreach($this->evaluations as $evaluation) {
            foreach($this->documents as $key => $document) {
                if ($evaluation->document != NULL) {
                    $similarity = $this->similarity($evaluation->document->dictionary, $document->dictionary);
                    if ($evaluation->evaluation === 1) 
                        $this->documents[$key]->score += $similarity;
                    else
                        $this->documents[$key]->score -= $similarity;
                }
            }
        }
    }

    public function getOrderedByRanking() 
    {   
        $original_ranking = $this->documents;
        $new_ranking = $this->documents->sortByDesc('score');

        return $this->calculateRanking($original_ranking, $new_ranking);
    }

    protected function calculateRanking($original_ranking, $new_ranking) {
        
        $original_pos = 1;
        foreach ($original_ranking as $original) {
            $current_pos = 1;
            foreach ($new_ranking as $key => $new) {
                if ($original->id == $new->id) {
                    $new_ranking[$key]->ranking = $original_pos - $current_pos;
                    continue;
                } else {
                    $current_pos++;
                }
            }

            $original_pos++;
        }

        return $new_ranking;
    }

    public function similarity(array $dictionary1, array $dictionary2)
    {

        $dotProduct = $this->_dotProduct($dictionary1, $dictionary2);
        $absVector1 = $this->_absVector($dictionary1);
        $absVector2 = $this->_absVector($dictionary2);

        return  $dotProduct / ( $absVector1 * $absVector2);
    }

    protected function _dotProduct(array $dictionary1, array $dictionary2)
    {
        $result = 0;

        foreach ($dictionary1 as $word1 => $occorencies1) {
            foreach ($dictionary2 as $word2 => $occorencies2) {
                if ($word1 === $word2) 
                    $result += $dictionary1[$word1] * $dictionary2[$word2];
            }
        }

        return $result;
    }

    protected function _absVector(array $vec)
    {
        $result = 0;

        foreach (array_values($vec) as $value) {
            $result += $value * $value;
        }

        return sqrt($result);
    }

    protected function _createDictionary($content) {
        $dictionary = [];
        $words = explode(" ", $content);

        foreach($words as $word) {
            $word = strtolower($word);

            if (in_array($word, $this->stopwords))
                continue;

            if(!isset($dictionary[$word])) {
                $dictionary[$word] = 1;
            } else {
                $dictionary[$word]++;
            }
        }
        
        return $dictionary;
    }

}