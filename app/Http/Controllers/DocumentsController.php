<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Document;

use Auth;

class DocumentsController extends Controller
{

    protected $client;

    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;
    }

    function index () 
    {
        $query = $this->client->createQuery($this->client::QUERY_SELECT);

        $result = $this->client->execute($query);

        $documents = Document::CreateCollectionFromQuery($result);

        return view('documents.index', compact('documents'));
    }

    function show($id)
    {
        $query = $this->client->createQuery($this->client::QUERY_SELECT);

        $query->setQuery('id:'. $id);

        $result = $this->client->execute($query);

        if ($result->getNumFound() > 0) {

            $document = new Document;

            $document->fillAttributes($result);

            return view('documents.show', compact('document'));
        }
    }


    function create()
    {
        return view('documents.create');
    }

    function store() 
    {
        $input = request()->input();
        $user_id = Auth::id();
        $document_id = time();

        $update = $this->client->createUpdate();

        $doc = $update->createDocument();
        
        $doc->user_id   = $user_id;
        $doc->title     = $input['title'];
        $doc->content   = $input['content'];


        $update->addDocuments(array($doc));
        $update->addCommit();

        $result = $this->client->update($update);

        return \Redirect::to('/documents');
    }

    function edit($id)
    {

        $query = $this->client->createQuery($this->client::QUERY_SELECT);

        $query->setQuery('id:'. $id);

        $result = $this->client->execute($query);

        if ($result->getNumFound() > 0) {

            $document = new Document;

            $document->fillAttributes($result);

            return view('documents.edit', compact('document'));
        }
        
    }

    function update($id)
    {
        $input = request()->input();
        $document_id = $id;

        $update = $this->client->createUpdate();

        $doc = $update->createDocument();
        
        $doc->id        = $document_id;
        $doc->title     = $input['title'];
        $doc->content   = $input['content'];

        $update->addDocuments(array($doc));
        $update->addCommit();

        $result = $this->client->update($update);

        return \Redirect::to('/documents');
    }

    function destroy($id)
    {
        $update = $this->client->createUpdate();
        
        $update->addDeleteById($id);
        $update->addCommit();

        $result = $this->client->update($update);

        return \Redirect::to('/documents');
    }
}
