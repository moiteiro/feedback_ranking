<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Input;
use Redirect;
use View;

class SessionController extends Controller {

    use AuthenticatesUsers;

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::user())
            return Redirect::home();
        return View::make('session.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::only('email', 'password');

        if (Auth::attempt($input, Input::get('remember'))) {
            return Redirect::home();
        }
        
        return Redirect::back()->withInput()->withFlashMessage('Invalid credentials provided');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id = null)
    {
        Auth::logout();

        return Redirect::home();
    }


}
