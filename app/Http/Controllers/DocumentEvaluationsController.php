<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Document;
use App\Models\DocumentEvaluation;

use Auth;
use Input;

class DocumentEvaluationsController extends Controller
{

    protected $client;

    protected $evaluation;

    public function __construct(
        \Solarium\Client $client,
        DocumentEvaluation $evaluation
        )
    {
        $this->client = $client;

        $this->evaluation = $evaluation;

        $this->middleware('auth');
    }

    function store() 
    {
        $input = request()->input();
        
        $data = [
            'document_id' => $input['document_id'],
            'user_id' => Auth::id(),
            'query' => $input['query'],
            'evaluation' => (int) $input['evaluation']
        ];

        $evaluation = $this->evaluation->exists(Auth::id(), $input['document_id'], $input['query']);
        
        if ($evaluation && $evaluation->evaluation == $input['evaluation']) {
            $result = $evaluation->delete();
            return $result ? response()->json(['status' => 'deleted']) : null;
        } elseif ($evaluation) {
            $result = $evaluation->fill($data)->save();
            return $result ? response()->json(['status' => 'updated']) : null;
        } else {
            return $this->evaluation->create($data);
        }
    }
}
