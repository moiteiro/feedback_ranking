<?php

namespace App\Http\Controllers;

use Acme\Forms\FormValidationException;
use App\Models\User;
use Redirect;
use Input;

class RegistrationController extends Controller {

    protected $user;

    public function __construct(User $user) 
    {
        $this->user = $user;

        $this->middleware('auth', ['only' => ['update', 'edit']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (\Auth::check()) {
            return \Redirect::home();
        }

        return \View::make('registration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $input = \Input::only('name', 'email', 'password', 'password_confirmation');

        $user = User::create($input);

        \Auth::login($user);

        return Redirect::home();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {
        if(\Auth::check())
            $user = \Auth::user()->id;

        return \View::make('registration.show', ['user', $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $user = User::find($id);
        if ($user == Auth::user()) {
            $input = Input::only('name', 'surname', 'password_current', 'password', 'password_confirmation');

            $form = $this->registrationForm->validateOnly($input, ['name', 'surname']);
            $user->fill(['name' => $input['name'], 'surname' => $input['surname']]);

            if ($input['password_current'] && Hash::check($input['password_current'],  Auth::user()->password))
            {
                $form = $this->registrationForm->validateOnly($input, ['password']);
                $user->fill(['password' => $input['password']]);
            }

            $user->save();
            Flash::success(trans('site.notifications.success'));
        }
        return Redirect::to('users/'.$id.'/edit')->withUser(Auth::user());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}