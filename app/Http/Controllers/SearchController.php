<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Classes\Rocchio;
use App\Models\Document;
use App\Models\DocumentEvaluation;

use Auth;

class SearchController extends Controller
{
    protected $client;

    public function __construct(\Solarium\Client $client)
    {
        $this->client = $client;

    }

    public function index()
    {
        $input = request()->input();

        $documents = [];

        if (isset($input['search']) && trim($input['search']) != "") {

            $query = $this->client->createSelect();

            $query->addFilterQuery(array('key'=>'title', 'query'=>$input['search']));
            $query->addFilterQuery(array('key'=>'content', 'query'=>$input['search']));

            $query->setFields(array('id','title','content', 'score'));

            $query->setRows(100);

            $result = $this->client->select($query);

            $highlighting = $result->getHighlighting();

            $statistics['total'] = $result->getNumFound();

            $original_documents = $documents = Document::CreateCollectionFromQuery($result);

            $evaluations = [];

            if (!Auth::guest()) {
                $evaluations = DocumentEvaluation::GetAllFromQuery(Auth::id(), $input['search'], $documents);
                
                if (!$evaluations->isEmpty()) {
                    $rocchio = new Rocchio($documents, $evaluations);
                    $rocchio->calculateSimilarity();
                    $documents = $rocchio->getOrderedByRanking();
                }
            }

        }

        return view('search.index', compact('documents', 'original_documents', 'statistics', 'input', 'evaluations'));
    }
}
