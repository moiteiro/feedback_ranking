<?php 

function truncate($text, $size)
{
	return strlen($text) > $size ? substr($text, 0, $size)."..." : $text;
}

function hightlight($words, $text) 
{
	$searchingFor = "/" . $words . "/i";
	$replacePattern = "<b>$0</b>";
	return preg_replace($searchingFor, $replacePattern, $text);
}