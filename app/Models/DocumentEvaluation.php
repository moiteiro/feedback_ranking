<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentEvaluation extends Model
{
    protected $table = "document_evaluations";

    protected $fillable = 
    [
    	'document_id',
        'user_id',
        'evaluation',
        'query',
    ];

    /**
    * Checks if the evaluation for this document exists
    * @param integer $user_id
    * @param integer $document_id
    * @param  string $query the query used in the current search
    */
    public function exists($user_id, $document_id, $query)
    {
        return $this->where('document_id','=', $document_id)
            ->where('user_id', '=', $user_id)
            ->where('query', '=', $query)
            ->get()
            ->first();
    }

    /**
     * Returns the list of evaluations of a search.
     * @param integer $user_id
     * @param string $query the current query used in the search
     */
    public static function GetAllFromQuery($user_id, $query, $documents = [])
    {
        $evaluations = self::where('user_id', '=', $user_id)
            ->where('query', '=', $query)
            ->get();

        if (!empty($documents)) {
            foreach ($evaluations as $key => $evaluation) {
                foreach ($documents as $document) {
                    if ($evaluation->document_id == $document->id) {
                        $evaluations[$key]->document = $document;
                    }
                }
            }
        }
        return $evaluations;
    }

    protected function _associateDocument($documents)
    {
        foreach ($this->evaluations as $key => $evaluation) {
            foreach ($this->documents as $key => $document) {
                if ($evaluation->document_id == $document->id) {
                    $this->evaluations[$key]->document = $document;
                }
            }
        }
    }


    // =====================================
    // Relationships
    // =====================================

    public function document()
    {
        return $this->belongsTo(Document::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
