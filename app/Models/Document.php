<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = 
    [
    	'user_id',
    	'title',
    	'content',
        'dictionary',
        'raking',
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public static function CreateCollectionFromQuery($resultset)
    {
    	$documents = collect();

        foreach ($resultset as $document) {
            $temp_document = new self();

            // the documents are also iterable, to get all fields
            foreach ($document as $field => $value) {
                // this converts multivalue fields to a comma-separated string
                if (is_array($value)) {
                    $value = implode(', ', $value);
                }

                $temp_document[$field] = $value;
            }

            $temp_document['dictionary'] = [];
            $temp_document['ranking'] = 0;

            $documents->push($temp_document);
        }

        return $documents;
    }

    /**
     * Fill the attributes from the query result.
     */
    public function fillAttributes($result)
    {
    	foreach ($result as $doc) {
            // the documents are also iterable, to get all fields
            foreach ($doc as $field => $value) {
                // this converts multivalue fields to a comma-separated string
                if (is_array($value)) {
                    $value = implode(', ', $value);
                }
                $this->attributes[$field] = $value;
            }
        }
    }


    // =====================================
    // Relationships
    // =====================================

    public function evaluations()
    {
        return $this->belongsToMany(User::class, 'document_evaluations');
    }
}
