<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'SearchController@index']);

# Document
Route::resource('/documents', 'DocumentsController');

# Evaluate Document
Route::resource('document_evaluations', 'DocumentEvaluationsController');

# Search
Route::resource('/search', 'SearchController');

# Registration
Route::resource('registration', 'RegistrationController');
# Session
Route::get('/login', ['as' => 'login', 'uses' => 'SessionController@create']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'SessionController@destroy']);
Route::resource('session', 'SessionController', ['only' => ['create', 'store', 'destroy']]);

