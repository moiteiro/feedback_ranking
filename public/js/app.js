$().ready(function(){

    // =====================================
    // Evaluate Document
    // =====================================
    $('.document-evaluation').click(function () {
        var _that = this;
        $.ajax({
            type: 'POST',
            url: '/document_evaluations',
            data: { 
                'document_id': $(_that).attr('data-document-id'),
                'evaluation': $(_that).attr('data-document-evaluation'),
                'query': $(_that).attr('data-query')
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $(_that).siblings().addClass('inverted')
                $(_that).removeClass('inverted');
                if (data.hasOwnProperty("status") && data.status == "deleted") {
                    $(_that).addClass('inverted');
                }
            }
        })
    })

    // =====================================
    // Delete confirmation modal
    // =====================================

    $('.remove-entry-button').click(function() {

        var name = $(this).attr('data-entry-name');
        var id = $(this).attr('data-entry-id');

        $('#target_form_delete').html(id + ' - <strong>' + name + '</strong>');
        $('#remove_entry_confirmation_form').attr('action', $(this).attr('data-remove-url'));
    });
});