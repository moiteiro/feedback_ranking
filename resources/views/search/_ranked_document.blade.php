<?php 
$evaluation = isset($evaluations) && $evaluations != null ? 
    $evaluations->where('document_id', $document->id)->first() : 
    null ; 
?>

<div class="col-sm-12 document-summarized">
    <div class="ranked_document @if($documents_count < 10) {{"good-document"}} @endif">
        <h4>{!! hightlight($input['search'], truncate($document->title, 50)) !!}</h4>
        <p>{!! hightlight($input['search'], truncate($document->content, 260)) !!}</p>
        @if (!Auth::guest())
        <span>
            <button 
                class="document-evaluation btn btn-xs btn-success
                @if( !($evaluation && $evaluation->evaluation == "1") )
                    inverted
                @endif
                "
                data-query="{{$input['search']}}"
                data-document-id="{{$document->id}}" 
                data-document-evaluation="1">
                Like
            </button>
            <button
                class="document-evaluation btn btn-xs btn-danger
                @if( !($evaluation && $evaluation->evaluation == "0") )
                    inverted
                @endif
                "
                data-query="{{$input['search']}}"
                data-document-id={{$document->id}} 
                data-document-evaluation="0" >
                Dislike
            </button>
        </span>
        <span style="font-size:14px; text-decoration: underline;">Relevance: {{ round($document->score, 3)}}</span>
        <span style="font-size:14px; text-decoration: underline;" title="Difference between previous and current position">Raking: 
            @if ($document->ranking > 0) 
            + 
            @endif
        {{ $document->ranking }}</span>
        @endif
    </div>
</div>