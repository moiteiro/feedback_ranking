@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h2 id="typography">Search</h2>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3"> 
            <form method="get" action="search">

                <div class="form-group">
                    <div class="input-group">
                        <input class="form-control" type="text" name="search" placeholder="Enter your search"
                            @if (isset($input['search']))
                                value="{{$input['search']}}"
                            @endif
                        >
                        <span class="input-group-btn">
                            <input type="submit" class="btn btn-primary" value="Search">
                        </span>
                    </div>
                    @if (isset($statistics) && $statistics['total'] > 0)
                        <p class="help-block text-center">Total of results: {{$statistics['total']}}</p>
                    @elseif (isset($statistics) && $statistics['total'] == 0)
                        <p class="help-block text-center">No results for this search</p>
                    @endif
                </div>

            </form>
        </div>
    </div>

    <div class="row">
        @if(Auth::guest())
            <div class="col-md-12">
                @foreach($documents as $document)
                    @include('search/_document')
                @endforeach
            </div>
        @else
            @if (count($documents) > 0)
                <div id="ranked_documents_list" class="col-sm-6">
                    <h4 class="text-center">Ranked Result</h4>
                    <?php $documents_count = 0; ?>
                    @foreach($documents as $document)
                        @include('search/_ranked_document')
                        <?php $documents_count++; ?>
                    @endforeach
                </div>
                <div id="original_documents" class="col-sm-6">
                    <h4 class="text-center">Original Result</h4>
                    @foreach($original_documents as $document)
                        @include('search/_document')
                    @endforeach
                </div>
            @endif
        @endif
    </div>
</div>

@endsection

