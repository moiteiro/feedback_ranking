<?php 
$evaluation = isset($evaluations) && $evaluations != null ? 
    $evaluations->where('document_id', $document->id)->first() : 
    null ;
    
$document_size = Auth::guest() ? "col-md-6" : "col-md-12";
?>

<div class="{{$document_size}} document-summarized">
    <div>
        <h4>{!! hightlight($input['search'], truncate($document->title, 50)) !!}</h4>
        <p>{!! hightlight($input['search'], truncate($document->content, 260)) !!}</p>
    </div>
</div>