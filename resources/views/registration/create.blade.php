@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h2 id="typography">Registration</h2>
        </div>
      </div>
    </div>

    <div class="row">
    	<!-- Login -->
    	<div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    
                    {!! Form::open(['route' => 'session.store']) !!}


                        <div class="form-group">
                            {!! Form::label('email', 'Eamil' ,['class' => 'control-label']) !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter your email']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Password' ,['class' => 'control-label']) !!}
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter the password']) !!}
                        </div>


                        {!! Form::submit('Create account', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- Register -->
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Create Accout</div>
                <div class="panel-body">
                    
                    {!! Form::open(['route' => 'registration.store']) !!}

                    	<div class="form-group">
                            {!! Form::label('name', 'Name' ,['class' => 'control-label']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter your name']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email' ,['class' => 'control-label']) !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter your email']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Password' ,['class' => 'control-label']) !!}
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter the password']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Confirm Password' ,['class' => 'control-label']) !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm your password']) !!}
                        </div>

                        {!! Form::submit('Create account', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection