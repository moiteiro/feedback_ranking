<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Relevance Feedback</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Html::style('css/bootstrap.css') !!}
    {!! Html::style('css/theme.css') !!}
    {!! Html::style('css/custom.css') !!}
  </head>
  <body>
    @include('layouts/_navigation')

    @yield('content')

    @include('layouts/_footer')
    {!! Html::script('js/jquery-1.10.2.min.js')!!}
    {!! Html::script('js/bootstrap.js')!!}
    {!! Html::script('js/app.js')!!}
  </body>
</html>
