@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h2 id="typography">Document</h2>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                
                <div class="panel-body">
                    <h2>{{$document->title}}</h2>
                    <p>{{$document->content}}</p>
                    <hr>
                    <p><span>id: </span>{{$document->id}}</p>
                    <p><span>_version_: </span>{{$document->_version_}}</p>
                    <hr>
                    <p>
                        <a href='/documents/{{$document->id}}/edit' class='btn btn-primary btn-xs'>Edit</a>
                        <a href="#" class="btn btn-xs btn-danger">Remove</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection