@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-header">
          <h2 id="typography">Documents</h2>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Create Document</div>
                <div class="panel-body">
                    
                    {!! Form::model($document, ['method' => 'PATCH', 'url' => "documents/$document->id" , 'files' => true]) !!}
                    
                        <div class="form-group">
                            {!! Form::label('file', 'File' ,['class' => 'control-label']) !!}
                            {!! Form::file('file', null, ['class' => 'file optional']) !!}
                            <p class="help-block">Add a file instead of fill the form below.</p>
                        </div>

                        <hr>

                        <div class="form-group">
                            {!! Form::label('title', 'Title' ,['class' => 'control-label']) !!}
                            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter the title']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('content', 'Content' ,['class' => 'control-label']) !!}
                            {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
                        </div>

                        {!! Form::submit('Alter document', ['class' => 'btn btn-primary']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection