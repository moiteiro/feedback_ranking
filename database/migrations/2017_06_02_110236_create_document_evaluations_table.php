<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_evaluations', function (Blueprint $table) {

            $table->increments('id');
            $table->string('document_id');
            $table->integer('user_id')->unsigned();
            $table->string('query');
            $table->boolean('evaluation');
            $table->timestamps();

            $table->index('query');
            $table->index('document_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('document_evaluations');
    }
}
